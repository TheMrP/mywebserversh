#!/bin/sh

if [ $(id -u) != "0" ]; then
    echo "You must be the superuser to run this script" >&2
    exit 1
fi

echo '..:: ====== GET UPDATES  ===== ::..'
	apt update
	apt --assume-yes full-upgrade

echo '..:: ====== INSTALLING DEPENDECIES  ===== ::..'
	apt --assume-yes install virtualbox
	apt --assume-yes install php
	apt --assume-yes install git
	apt --assume-yes install mcrypt
	apt --assume-yes install php-mbstring
	apt --assume-yes install phpunit
	apt --assume-yes install php-bcmath
	apt --assume-yes install zip
	apt --assume-yes install php-gd
	apt --assume-yes install php-zip
	apt --assume-yes install php-mysql
	apt --assume-yes install php-curl