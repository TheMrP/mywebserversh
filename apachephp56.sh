#!/bin/sh

if [ $(id -u) != "0" ]; then
    echo "You must be the superuser to run this script" >&2
    exit 1
fi

echo '..:: ====== GET UPDATES  ===== ::..'
    add-apt-repository ppa:ondrej/php
    apt update
    apt --assume-yes full-upgrade

echo '..:: ====== INSTALLING DEPENDECIES  ===== ::..'
    apt --assume-yes install fail2ban
    apt --assume-yes install apache2
    apt --assume-yes install php5.6
    apt --assume-yes install git
    apt --assume-yes install ufw
    apt --assume-yes install mcrypt
    apt --assume-yes install php5.6-mbstring
    apt --assume-yes install phpunit
    apt --assume-yes install php5.6-bcmath
    apt --assume-yes install zip
    apt --assume-yes install libapache2-mod-security2
    apt --assume-yes install php5.6-gd
    apt --assume-yes install php5.6-zip
    apt --assume-yes install php5.6-mysql
    apt --assume-yes install php5.6-curl
    apt --assume-yes remove telnet

echo '..:: ====== WANT TO ACTIVE MODSECURITY [ y / n ] ===== ::..'
echo '..:: ====== SecRuleEngine = XXX -> SecRuleEngine = on ===== ::..'
    read RESPONSE_MODSECURITY
    if [ "$RESPONSE_MODSECURITY" = "y" ] || [ "$RESPONSE_MODSECURITY" = "Y" ]; then
        #Move recomended configuration to mod security
        cp /etc/modsecurity/modsecurity.conf-recommended /etc/modsecurity/modsecurity.conf
        nano /etc/modsecurity/modsecurity.conf
    fi

echo '..:: ====== WANT TO EDIT PHP.INI [ y / n ] ===== ::..'
    read RESPONSE_PHPINI
    if [ "$RESPONSE_PHPINI" = "y" ] || [ "$RESPONSE_PHPINI" = "Y" ]; then
        #Move recomended configuration to mod security
        nano /etc/php/7.2/apache2/php.ini
    fi

echo '..:: ====== ENABLING THE FIREWALL  ===== ::..'
    ufw allow 'Apache Full'
    ufw allow 'OpenSSH'
    ufw enable

echo '..:: ====== EXTRA SECURITY RECOMENDATIONS  ===== ::..'
    #USE IPTABLES TO BLOCK COMMON ATTACKS
    #Syn-flood protection
    iptables -A INPUT -p tcp ! --syn -m state --state NEW -j DROP
    #Incoming malformed XMAS packets drop them
    iptables -A INPUT -p tcp --tcp-flags ALL ALL -j DROP
    #Incoming malformed NULL packets
    iptables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP
    #Drop incoming packets with fragments
    iptables -A INPUT -f -j DROP

echo '..:: ====== WANT TO DEACTIVATE THE EXTRA INFORMATION RESPONSE FROM APACHE [ y / n ] ===== ::..'
echo '..:: ====== ServerTokens XXXX     ->  ServerTokens Prod  ===== ::..'
echo '..:: ====== ServerSignature XXXX  ->  ServerSignature Off  ===== ::..'
echo '..:: ====== TraceEnable XXXX      -> TraceEnable Off  ===== ::..'
    read RESPONSE_SECURITY

    if [ "$RESPONSE_SECURITY" = "y" ] || [ "$RESPONSE_SECURITY" = "Y" ]; then
      nano /etc/apache2/conf-enabled/security.conf
    fi

echo 'ENTER YOUR EMAIL'
    read ADMIN_MAIL

echo 'ENTER THE NAME OF YOUR INITIAL PROJECT (myproject.domain )'
    read PROJECT_NAME

echo '..:: ====== CREATING THE SCAFFOLDING FOR VIRTUALHOST  ===== ::..'
    mkdir -p /var/www/${PROJECT_NAME}/public
    chown -R $SUDO_USER:$SUDO_USER /var/www/${PROJECT_NAME} # set the folder own to user invoke sudo
    chmod -R 755 /var/www/${PROJECT_NAME}

    echo  '<VirtualHost *:80>' >> /etc/apache2/sites-available/${PROJECT_NAME}.conf
    echo  '    ServerAdmin '${ADMIN_MAIL}  >> /etc/apache2/sites-available/${PROJECT_NAME}.conf
    echo  '    ServerName '${PROJECT_NAME}  >> /etc/apache2/sites-available/${PROJECT_NAME}.conf
    echo  '    ServerAlias www.'${PROJECT_NAME}  >> /etc/apache2/sites-available/${PROJECT_NAME}.conf
    echo  '    DocumentRoot /var/www/'${PROJECT_NAME}'/public'  >> /etc/apache2/sites-available/${PROJECT_NAME}.conf
    echo  '    ErrorLog ${APACHE_LOG_DIR}/error.log'  >> /etc/apache2/sites-available/${PROJECT_NAME}.conf
    echo  '    CustomLog ${APACHE_LOG_DIR}/access.log combined' >> /etc/apache2/sites-available/${PROJECT_NAME}.conf
    echo  '</VirtualHost>' >> /etc/apache2/sites-available/${PROJECT_NAME}.conf

    a2ensite ${PROJECT_NAME}.conf
    a2dissite 000-default.conf

#echo '..:: ====== ADDING A CERTBOT ===== ::..'
#add-apt-repository ppa:certbot/certbot -y
#apt install python-certbot-apache
#sudo certbot --test-cert --apache -d ${PROJECT_NAME} -d www.${PROJECT_NAME}
#echo '..:: ====== ADDING MARIADB ===== ::..'
#apt install mariadb-server
echo '..:: ====== RESTARTING APACHE CAN TEST IN THE IP FROM SERVER ===== ::..'
systemctl restart apache2
