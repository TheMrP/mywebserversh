#!/bin/bash

wget -O champp.run https://www.apachefriends.org/xampp-files/7.2.10/xampp-linux-x64-7.2.10-0-installer.run
chmod 777 champp.run
sudo ./champp.run
cd /opt/lampp/phpmyadmin
sudo mkdir .tmp
sudo chmod 777 .tmp
cd /opt/lampp/
sudo chmod 777 htdocs
sudo ln -s /opt/lampp/bin/php /usr/local/bin/php